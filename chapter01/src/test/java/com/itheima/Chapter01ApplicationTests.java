package com.itheima;

import com.itheima.controller.HelloController;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * IDEA开发工具快捷方式自动创建的主程序启动类对应的单元测试类
 */

// 1. 测试类必须在主启动类所在包路径同级下或其子级下，否则无法扫描到bean，且无法注入需要的bean，会报错
// 2. @RunWith(SpringRunner.class)和 @SpringBootTest注解的意义在于标记为Spring Boot单元测试类，并加载项目的ApplicationContext上下文环境
//    在springboot 2.2版本之后只需要贴注解@SpringBootTest, @Test注解为org.junit.jupiter.api.Test

//@RunWith(SpringRunner.class)
@SpringBootTest
//@ActiveProfiles("Test")
public class Chapter01ApplicationTests {
    @Autowired
    private HelloController helloController;

    // 自动创建的单元测试方法示例
    @Test
    public void contextLoads() {
    }

    @Test
    public void helloControllerTest() {
        String hello = helloController.hello();
        System.out.println(hello);
    }


}
