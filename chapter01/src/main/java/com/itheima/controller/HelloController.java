package com.itheima.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @Classname HelloController
 * @Description TODO
 * @Date 2019-3-1 13:50
 * @Created by CrazyStone
 */

//该注解为组合注解，等同于Spring中@Controller+@ResponseBody注解
@RestController
public class HelloController {
    //该注解等同于Spring框架中@RequestMapping(RequestMethod.GET)注解
    @GetMapping("/hello")
    public String hello(){
        return "你好，Spring Boot!!";
    }
}

