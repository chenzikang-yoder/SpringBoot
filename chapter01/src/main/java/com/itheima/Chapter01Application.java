package com.itheima;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.logging.Logger;

//@Slf4j: 为了能够少写两行代码，不用每次都在类的最前边写上,
//private static final Logger logger = LoggerFactory.getLogger(this.XXX.class);
//@SpringBootApplication 标注启动类，
@Slf4j
@SpringBootApplication
public class Chapter01Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext ioc = SpringApplication.run(Chapter01Application.class, args);

        //查看容器内的组件
        String[] names = ioc.getBeanDefinitionNames();
        for(String name : names) {
            System.out.println(name);
        }

        //Logger.getLogger("Chapter01").info("test ok");
        log.info("log对象是lombok + @Slf4j 注解自动生成的。");
    }

}
